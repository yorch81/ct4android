# CT4Android #

## Descripción ##
CadastreTools Android Demo

## Requerimientos ##
* [Java](https://www.java.com/es/download/)
* [Android Studio](https://developer.android.com/sdk/index.html)
* [Android Bootstrap](https://github.com/Bearded-Hen/Android-Bootstrap)
* [netWf](https://bitbucket.org/yorch81/netwf)
* [netWf4Android](https://bitbucket.org/yorch81/netwf_android)

## Documentación ##
Se puede generar desde Android Studio.

## Instalación ##
La configuración de Android por default solo permite instalar aplicaciones desde Google Play, para poder instalar el archivo apk generado deberá activar la instalación de Origenes Desconocidos (Unknow Sources) en el celular anteriormente.

## Notas ##
Esta aplicación se conecta a netDb Web Framework

## Referencias ##
http://developer.android.com/design/index.html

## Licencia ##
http://www.apache.org/licenses/LICENSE-2.0

P.D. Let's go play !!!