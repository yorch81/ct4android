package net.yorch.android.ct4android;

import android.content.pm.ActivityInfo;
import android.net.Uri;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.beardedhen.androidbootstrap.BootstrapButton;
import com.google.android.gms.appindexing.Action;
import com.google.android.gms.appindexing.AppIndex;
import com.google.android.gms.common.api.GoogleApiClient;

import net.yorch.android.netWf;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class MainActivity extends AppCompatActivity {

    netWf nw = null;
    String nwUrl = "http://148.237.75.122:9191";
    String nwKey = "12345678987654321";

    boolean status = true;
    /**
     * ATTENTION: This was auto-generated to implement the App Indexing API.
     * See https://g.co/AppIndexing/AndroidStudio for more information.
     */
    private GoogleApiClient client;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // Vertical
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        // Resuelve error en Android 4.0
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        nw = new netWf(nwUrl, nwKey);
        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        client = new GoogleApiClient.Builder(this).addApi(AppIndex.API).build();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    public void getPadron(View view) {
        EditText txtcve = (EditText) findViewById(R.id.txtCveCat);
        BootstrapButton btnBuscar = (BootstrapButton) findViewById(R.id.btnBuscar);

        if (txtcve.getText().toString().length() == 8 || txtcve.getText().toString().length() == 12){
            if (status) {
                btnBuscar.setBootstrapType("warning");
                btnBuscar.setText("Limpiar");
                btnBuscar.setLeftIcon("fa-refresh");
                status = false;

                JSONObject jParam = new JSONObject();

                JSONObject jResult = null;

                try {
                    jParam.put("@cve_cat", txtcve.getText().toString());

                    jResult = nw.procedure("ct_sp_getPadron", jParam);
                } catch (JSONException e) {
                    Toast.makeText(getApplicationContext(), "No existe conexión con el Servidor", Toast.LENGTH_LONG).show();
                    e.printStackTrace();
                }
                if (jResult.length() == 0)
                    jResult = null;

                showInfo(jResult);
            } else {
                showInfo(null);

                btnBuscar.setBootstrapType("success");
                btnBuscar.setText("Consultar");
                btnBuscar.setLeftIcon("fa-search");
                status = true;

                txtcve.setText("");
            }
        }
        else
            Toast.makeText(getApplicationContext(), "Clave Catrastral Inválida", Toast.LENGTH_LONG).show();
    }

    public void showInfo(JSONObject data) {
        EditText txtProp = (EditText) findViewById(R.id.txtPropietario);
        EditText txtDirec = (EditText) findViewById(R.id.txtDirec);
        EditText txtUbica = (EditText) findViewById(R.id.txtUbica);
        EditText txtTerreno = (EditText) findViewById(R.id.txtValorTerr);
        EditText txtConst = (EditText) findViewById(R.id.txtValorConst);
        EditText txtValorCat = (EditText) findViewById(R.id.txtValorCat);
        EditText txtAdeudo = (EditText) findViewById(R.id.txtAdeudo);

        if (data == null) {
            txtProp.setText("");
            txtDirec.setText("");
            txtUbica.setText("");
            txtTerreno.setText("");
            txtConst.setText("");
            txtValorCat.setText("");
            txtAdeudo.setText("");
        } else {
            JSONObject jRow = null;

            try {
                JSONArray jArr = data.getJSONArray("Table");

                if (jArr.length() > 0){
                    jRow = jArr.getJSONObject(0);

                    txtProp.setText(jRow.getString("Nombre"));
                    txtDirec.setText(jRow.getString("UbicacionPropietario"));
                    txtUbica.setText(jRow.getString("Ubicacion"));
                    txtTerreno.setText(String.format("%.2f", jRow.getDouble("Valor_terreno")));
                    txtConst.setText(String.format("%.2f", jRow.getDouble("Valor_construccion")));
                    txtValorCat.setText(String.format("%.2f", jRow.getDouble("Valor_catastral")));
                    txtAdeudo.setText(String.format("%.2f", jRow.getDouble("ImpuestoPredial")));
                }
                else
                    Toast.makeText(getApplicationContext(), "La clave catastral no existe", Toast.LENGTH_LONG).show();

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void onStart() {
        super.onStart();

        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        client.connect();
        Action viewAction = Action.newAction(
                Action.TYPE_VIEW, // TODO: choose an action type.
                "Main Page", // TODO: Define a title for the content shown.
                // TODO: If you have web page content that matches this app activity's content,
                // make sure this auto-generated web page URL is correct.
                // Otherwise, set the URL to null.
                Uri.parse("http://host/path"),
                // TODO: Make sure this auto-generated app deep link URI is correct.
                Uri.parse("android-app://net.yorch.android.ct4android/http/host/path")
        );
        AppIndex.AppIndexApi.start(client, viewAction);
    }

    @Override
    public void onStop() {
        super.onStop();

        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        Action viewAction = Action.newAction(
                Action.TYPE_VIEW, // TODO: choose an action type.
                "Main Page", // TODO: Define a title for the content shown.
                // TODO: If you have web page content that matches this app activity's content,
                // make sure this auto-generated web page URL is correct.
                // Otherwise, set the URL to null.
                Uri.parse("http://host/path"),
                // TODO: Make sure this auto-generated app deep link URI is correct.
                Uri.parse("android-app://net.yorch.android.ct4android/http/host/path")
        );
        AppIndex.AppIndexApi.end(client, viewAction);
        client.disconnect();
    }
}
